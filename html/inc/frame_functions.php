<?php
function top($title = "Parkour One")
{
?>
    <!DOCTYPE html>
    <html lang="de">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Parkourone Scoreboard">
        <meta name="author" content="Stefan Zumdick">
        <title><?php echo $title; ?></title>
        <link rel="icon" href="favicon.ico">
        <link rel="apple-touch-icon" href="apple-touch-icon.jpg" sizes="180x180">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Custom styles for this template -->
        <link href="css/nav-bottom.css" rel="stylesheet">
    </head>

<?php
    echo '<body>';
}

function start_main()
{
    echo '<main role="main" class="container">';

    // Display Alerts
    if (isset($_SESSION["alert_array"])) {
        foreach ($_SESSION["alert_array"] as $alert) {
            if (isset($alert["message"])) echo '<div class="row"><div class="col-md-6 offset-md-3"><div class="alert alert-' . $alert["type"] . '" role="alert">' . $alert["message"] . '</div></div></div>';
        }
        unset($_SESSION["alert_array"]);
    }

    // Remember visited pages
    // 0 = oldest element, logout
    // n = newest element, current page
    if (isset($_SESSION["sites_visited"])) {
        if (end($_SESSION["sites_visited"]) != $_SERVER['REQUEST_URI']) {

            // Check if user went back
            if (isset($_SESSION["sites_visited"][count($_SESSION["sites_visited"]) - 2]) && $_SESSION["sites_visited"][count($_SESSION["sites_visited"]) - 2] == $_SERVER['REQUEST_URI']) array_pop($_SESSION["sites_visited"]);
            else $_SESSION["sites_visited"][] = $_SERVER['REQUEST_URI'];
        }
    }
}

function bot()
{
?>
    <br>
    </main>
    <script src="js/jquery-3.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')
    </script>
    <script src="js/bootstrap.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

    </body>

    </html>
<?php
}
?>