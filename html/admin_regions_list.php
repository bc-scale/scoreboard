<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if ($_SESSION["user_admin_level"] < 1) {
    header("Location: access_denied.php");
    exit;
};

// Build regions-array
$result = $mysqli->query("SELECT id, name FROM regions ORDER BY name");
while ($region = $result->fetch_object()) {
    $result_user = $mysqli->query("SELECT COUNT(*) FROM users WHERE region = '$region->id';");
    $region->count_user = $result_user->fetch_row()[0];
    $regions_array[] = $region;
}

top("Regionen verwalten");
nav(build_nav($mysqli), "Regionen verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Regionen verwalten</h3>
        <br>
        <?php
        if (isset($regions_array)) {
        ?>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Anzahl Nutzer</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($regions_array as $region) {
                        echo '<tr><td>' . $i . '</td>';
                        if (may_edit_region($region)) echo '<td><a href="admin_regions_edit.php?region_id=' . $region->id . '">' . $region->name . '</a></td>';
                        else echo '<td scope="row">' . $region->name . '</td>';
                        echo '<td scope="row">' . $region->count_user . '</td>';
                        echo '</tr>';
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        <?php
        }
        if ($_SESSION["user_admin_level"] == 1) echo '<div class="form-group"><a href="admin_regions_add.php" class="btn btn-primary btn-block">Neue Region hinzufügen</a></div>';
        echo back_button();
        ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>