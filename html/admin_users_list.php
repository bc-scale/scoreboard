<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if ($_SESSION["user_admin_level"] < 1) {
    header("Location: access_denied.php");
    exit;
};

// Build users-array
$query = "SELECT * FROM users";

// If user is a region admin only show users of his region
if ($_SESSION["user_admin_level"] == 2) $query .= " WHERE region = $_SESSION[user_region]";
$result = $mysqli->query($query . " ORDER BY name") or die($mysqli->error);
while ($user = $result->fetch_object()) {
    $users_array[] = $user;
}

top("Nutzer verwalten");
nav(build_nav($mysqli), "Nutzer verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Nutzer verwalten</h3>
        <br>
        <?php
        if (isset($users_array)) {
        ?>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Region</th>
                        <th scope="col">Berechtigung</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($users_array as $user) {
                        echo '<tr>';
                        echo '<td scope="row">' . $i . '</td>';
                        if (may_edit_user($user)) echo '<td scope="row"><a href="admin_users_edit.php?user_id=' . $user->id . '">' . $user->name . '</a></td>';
                        else echo '<td scope="row">' . $user->name . '</td>';
                        if ($_SESSION["user_admin_level"] == 1) echo '<td><a href="admin_regions_edit.php?region_id=' . $user->region . '">' . get_region_by_id($mysqli, $user->region) . '</a></td>';
                        else echo '<td scope="row">' . get_region_by_id($mysqli, $user->region) . '</td>';
                        echo '<td scope="row">' . get_admin_description($mysqli, $user) . '</td>';
                        echo '</tr>';
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        <?php
        }
        ?>
        <div class="form-group">
            <a href="admin_users_add.php" class="btn btn-primary btn-block">Neuen Nutzer hinzufügen</a>
        </div>
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>