<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

// Get information of selected user
$result_user = $mysqli->query("SELECT name, region, id FROM users WHERE id = '$_GET[user_id]'");
if ($result_user->num_rows == 1) {
    $user = $result_user->fetch_object();

    // Check if user is allowed to see stats
    if ($user->region != $_SESSION['user_region'] && $_SESSION["user_admin_level"] != 1) {
        header("Location: access_denied.php");
        exit;
    } else {
        $score_array = array();
        $score_user = 0;
        $score_average_per_day = 0;

        // Get all entries of selected user
        $query_user_stats = "SELECT CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp, count, value FROM stats LEFT JOIN exercises ON stats.exercise_id = exercises.id WHERE user_id = '$user->id'";
        if ($_SESSION["exercise_id"] != 0) $query_user_stats .= " AND exercise_id = '$_SESSION[exercise_id]'";
        $query_user_stats .= " ORDER BY stats.id DESC";
        $result_user_stats = $mysqli->query($query_user_stats);
        if ($result_user_stats->num_rows > 0) {
            $score_array = array();
            $score_per_weekday_array = array("Montag" => 0, "Dienstag" => 0, "Mittwoch" => 0, "Donnerstag" => 0, "Freitag" => 0, "Samstag" => 0, "Sonntag" => 0);

            while ($entry = $result_user_stats->fetch_object()) {
                $date = new DateTime($entry->timestamp);
                if (empty($score_array[$date->format('d.m.Y')])) $score_array[$date->format('d.m.Y')] = $entry->count * $entry->value;
                else $score_array[$date->format('d.m.Y')] += $entry->count * $entry->value;
                switch ($date->format('l')) {
                    case "Monday":
                        $score_per_weekday_array["Montag"] += $entry->count * $entry->value;
                        break;
                    case "Tuesday":
                        $score_per_weekday_array["Dienstag"] += $entry->count * $entry->value;
                        break;
                    case "Wednesday":
                        $score_per_weekday_array["Mittwoch"] += $entry->count * $entry->value;
                        break;
                    case "Thursday":
                        $score_per_weekday_array["Donnerstag"] += $entry->count * $entry->value;
                        break;
                    case "Friday":
                        $score_per_weekday_array["Freitag"] += $entry->count * $entry->value;
                        break;
                    case "Saturday":
                        $score_per_weekday_array["Samstag"] += $entry->count * $entry->value;
                        break;
                    case "Sunday":
                        $score_per_weekday_array["Sonntag"] += $entry->count * $entry->value;
                        break;
                }
                $score_user += $entry->count * $entry->value;
            }

            // Get max. scores
            $max_all_time = 0;
            $max_per_weekday = 0;
            foreach ($score_array as $daily_score) if ($daily_score > $max_all_time) $max_all_time = $daily_score;
            foreach ($score_per_weekday_array as $daily_score) if ($daily_score > $max_per_weekday) $max_per_weekday = $daily_score;

            // Get average
            $query_first_day = "SELECT CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp FROM stats WHERE user_id = '$user->id'";
            if ($_SESSION["exercise_id"] != 0) $query_first_day .= " AND exercise_id = '$_SESSION[exercise_id]'";
            $query_last_day = $query_first_day . " ORDER BY id DESC LIMIT 1";
            $query_first_day .= " ORDER BY id ASC LIMIT 1";
            $result_first_day = $mysqli->query($query_first_day);
            $first_day = new DateTime($result_first_day->fetch_object()->timestamp);
            $result_last_day = $mysqli->query($query_last_day);
            $last_day = new DateTime($result_last_day->fetch_object()->timestamp);
            $score_average_per_day = round($score_user / ($last_day->diff($first_day)->format("%a") + 1));
        }
    }
} else {
    header("Location: access_denied.php");
    exit;
}

top("Scoreboard");
nav(build_nav($mysqli), "Statistik");
start_main();
nav_stats($mysqli);
?>

<div class="row">
    <div class="col offset-md-3">
        <?php
        nav_exercises($mysqli);
        ?>
        <table class="table table-sm">
            <tr>
                <td style="border-top:none">Gesamtpunktzahl</td>
                <td style="border-top:none" class="text-right"><?php echo $score_user; ?></td>
            </tr>
            <tr>
                <td>Tagesdurchschnitt</td>
                <td class="text-right"><?php echo $score_average_per_day; ?></td>
            </tr>
        </table>
        <?php
        if ($score_user > 0) {
        ?>
            <table class="table table-sm">
                <tr>
                    <th>Wochentag</th>
                    <th class="text-center">Score</th>
                    <th></th>
                </tr>
                <?php
                foreach ($score_per_weekday_array as $day => $daily_score) {
                    echo '<tr class="small">';
                    echo '<td>' . $day . '</td>';
                    echo '<td><svg width="100%" height="15px"><rect style="fill: hsl(120, 50%, 40%)" width="' . $daily_score / $max_per_weekday * 100 . '%" height="10"></rect></svg></td>';
                    echo '<td class="text-right">' . $daily_score . '</td>';
                    echo '</tr>';
                }
                ?>
            </table>
            <br>
            <table class="table table-sm">
                <caption><?php echo $user->name; ?></caption>
                <thead>
                    <tr>
                        <th>Datum</th>
                        <th class="text-center">Score</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($score_array as $date => $score) {
                        echo '<tr class="small">';
                        echo '<td>' . $date . '</td>';
                        echo '<td><svg width="100%" height="15px"><rect style="fill: hsl(120, 50%, 40%)" width="' . $score / $max_all_time * 100 . '%" height="10"></rect></svg></td>';
                        echo '<td class="text-right">' . $score . '</td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        <?php
        } else echo '<div class="alert alert-primary" role="alert">' . $user->name . ' hat bislang keine ' . get_exercise_by_id($mysqli, $_SESSION["exercise_id"])->name . ' eingetragen.</div>';
        echo back_button();
        ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>