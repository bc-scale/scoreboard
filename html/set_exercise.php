<?php
include('inc/config.php');
include('inc/functions.php');

start($mysqli);

if (isset($_GET["exercise_id"])) {
    if ($_GET["exercise_id"] == 0) $_SESSION["exercise_id"] = $_GET["exercise_id"];
    else {
        $result = $mysqli->query("SELECT id FROM exercises WHERE region_id = '$_SESSION[user_region]';");
        while ($row = $result->fetch_object()) {
            if ($row->id == $_GET["exercise_id"]) $_SESSION["exercise_id"] = $_GET["exercise_id"];
        }
    }
}

header("Location: " . array_pop($_SESSION["sites_visited"]));
exit;
