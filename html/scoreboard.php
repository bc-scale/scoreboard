<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

// Create current date object
$current_date = new DateTime();
$current_date->setTimezone(new DateTimeZone($timezone_user));

// Get region
$result_goal = $mysqli->query("SELECT * FROM regions WHERE id = $_SESSION[user_region]");
$region = $result_goal->fetch_object();

// Get user array
$result_users = $mysqli->query("SELECT id, name FROM users ORDER BY name;");
if ($result_users->num_rows > 0) {
    while ($user = $result_users->fetch_object()) {
        $users_array[$user->id]["name"] = $user->name;
        $users_array[$user->id]["timestamp"] = $current_date;
    }
}

// Get stats
$region_score_total = 0;
$query_stats = "SELECT CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp, user_id FROM stats WHERE region_id = '$_SESSION[user_region]'";
if ($_SESSION["exercise_id"] != 0) $query_stats .= " AND exercise_id = '$_SESSION[exercise_id]'";
$query_stats .= " ORDER BY timestamp DESC;";
$result_stats = $mysqli->query($query_stats);
if ($result_stats->num_rows > 0) {
    // Create $score_array
    $score_array = array();
    while ($entry = $result_stats->fetch_object()) {
        $date = new DateTime($entry->timestamp);
        $score_array[$date->format('m.Y')][$entry->user_id] = 0;
    }

    // Sort entries
    $query_stats = "SELECT value, count, CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp, user_id FROM stats LEFT JOIN exercises on stats.exercise_id = exercises.id WHERE stats.region_id = '$_SESSION[user_region]'";
    if ($_SESSION["exercise_id"] != 0) $query_stats .= " AND exercise_id = '$_SESSION[exercise_id]';";
    $result_stats = $mysqli->query($query_stats) or die($mysqli->error);
    while ($entry = $result_stats->fetch_object()) {
        $date = new DateTime($entry->timestamp);

        // Monthly scores
        $score_array[$date->format('m.Y')][$entry->user_id] += $entry->count * $entry->value;

        // Total score
        $region_score_total += $entry->count * $entry->value;

        // Timestamp
        $users_array[$entry->user_id]["timestamp"] = $date;
    }
}

top("Scoreboard - " . $region->name);
nav(build_nav($mysqli), "Scoreboard");
start_main();
nav_scoreboard();
?>

<div class="row">
    <div class="col offset-md-3">
        <?php
        nav_exercises($mysqli);
        ?>
        <p>Gesamtpunktzahl: <?php echo $region_score_total; ?></p>
        <?php
        if (isset($score_array)) {

            // Generate table for each month
            $is_first_month = true;
            foreach ($score_array as $month => $monthly_score_array) {
                arsort($monthly_score_array);

                // Get total score
                $region_score_total = 0;
                foreach ($monthly_score_array as $score) $region_score_total += $score;
        ?>
                <br>
                <h4><?php echo $month . ': ' . $region_score_total; ?></h4>
                <table class="table">
                    <caption><?php echo $region->name . ', ' . $month; ?></caption>
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Score</th>
                            <th scope="col">Name</th>
                            <?php if ($is_first_month == true) echo '<th scope="col">Datum</th>'; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        $o = 0;
                        $lastscore = 0;
                        foreach ($monthly_score_array as $user_id => $score) {
                            // If two users have the same score, they will share their placement
                            if ($lastscore != $score) {
                                $i++;
                                $i += $o;
                                $o = 0;
                            } else $o++;

                            echo '<tr>';
                            echo '<td scope="row">' . $i . '</td>';
                            echo '<td scope="row">' . $score . '</td>';
                            echo '<td scope="row"><a href="stats_user.php?user_id=' . $user_id . '">' . $users_array[$user_id]["name"] . '</a></td>';
                            if ($is_first_month == true) {
                                if ($current_date->format('Ymd') == $users_array[$user_id]["timestamp"]->format('Ymd')) {
                                    $timestamp = $users_array[$user_id]["timestamp"]->format("H:i") . ' Uhr';
                                } else {
                                    $timestamp = $users_array[$user_id]["timestamp"]->format("d.m.Y");
                                }
                                echo '<td scope="row">' . $timestamp . '</td>';
                            }
                            echo '</tr>';
                            $lastscore = $score;
                        }
                        ?>
                    </tbody>
                </table>
        <?php
                $is_first_month = false;
            }
        } else echo '<div class="alert alert-primary" role="alert">Leider hat bislang niemand aus deiner Region ' . get_exercise_by_id($mysqli, $_SESSION["exercise_id"])->name . ' eingetragen. Es wird Zeit das zu ändern!</div>';
        ?>
        <div class="form-group">
            <a href="scoreboard.php" class="btn btn-primary btn-block">Aktualisieren</a>
        </div>
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>