<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

// Create current date object
$current_date = new DateTime();
$current_date->setTimezone(new DateTimeZone($timezone_user));

// Get region
$result_region = $mysqli->query("SELECT name, challenge_goal, CONVERT_TZ(challenge_start, 'UTC', '$timezone_user') as challenge_start, CONVERT_TZ(challenge_end, 'UTC', '$timezone_user') as challenge_end, challenge_end as challenge_end_utc FROM regions WHERE id = $_SESSION[user_region]");
$region = $result_region->fetch_object();

// Get user array
$result_users = $mysqli->query("SELECT id, name FROM users ORDER BY name;");
if ($result_users->num_rows > 0) {
    while ($user = $result_users->fetch_object()) {
        $score_array[$user->id] = $user;
    }
}

// Get stats
$region->challenge_score = 0;
$region->challenge_score_today = 0;
$result_stats = $mysqli->query("SELECT CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp, user_id FROM stats WHERE region_id = '$_SESSION[user_region]' AND timestamp BETWEEN CONVERT_TZ('$region->challenge_start', '$timezone_user', 'UTC') AND CONVERT_TZ('$region->challenge_end', '$timezone_user', 'UTC') ORDER BY timestamp DESC;");
if ($result_stats->num_rows > 0) {
    while ($entry = $result_stats->fetch_object()) {
        $date = new DateTime($entry->timestamp);
        $score_array[$entry->user_id]->score = 0;
    }

    // Get entries
    $result_stats = $mysqli->query("SELECT
        exercise_id,
        count,
        CONVERT_TZ(timestamp, 'UTC', '$timezone_user') as timestamp,
        user_id,
        value
    FROM
        stats LEFT JOIN exercises on stats.exercise_id = exercises.id
    WHERE
        stats.region_id = '$_SESSION[user_region]'
    AND timestamp BETWEEN CONVERT_TZ('$region->challenge_start', '$timezone_user', 'UTC') AND CONVERT_TZ('$region->challenge_end', '$timezone_user', 'UTC');");

    // Create DateTime objects
    $region->challenge_start = new DateTime($region->challenge_start);
    $region->challenge_end = new DateTime($region->challenge_end);
    $region->challenge_end_utc = new DateTime($region->challenge_end_utc);

    // Sort entries
    while ($entry = $result_stats->fetch_object()) {
        $entry->timestamp = new DateTime($entry->timestamp);

        // Score and timestamp per user
        if ($entry->exercise_id == $_SESSION['exercise_id'] || $_SESSION["exercise_id"] == 0) {
            $score_array[$entry->user_id]->score += $entry->count * $entry->value;
            $score_array[$entry->user_id]->timestamp = $entry->timestamp;
        }

        // Daily score
        if ($current_date->format('Ymd') == $entry->timestamp->format('Ymd')) {
            $region->challenge_score_today += $entry->count * $entry->value;
        }

        // Challenge score
        $region->challenge_score += $entry->count * $entry->value;
    }
} else {
    // Create DateTime objects
    $region->challenge_start = new DateTime($region->challenge_start);
    $region->challenge_end = new DateTime($region->challenge_end);
    $region->challenge_end_utc = new DateTime($region->challenge_end_utc);
}

top("Scoreboard - Herausforderung");
nav(build_nav($mysqli), "Scoreboard");
start_main();
nav_scoreboard();
?>

<div class="row">
    <div class="col offset-md-3">
        <table class="table table-sm">
            <tr>
                <td style="border-top:none">Start</td>
                <td style="border-top:none"><?php echo $region->challenge_start->format('d.m.Y, H:i') ?> Uhr</td>
            </tr>
            <tr>
                <td>Ende</td>
                <td><?php echo $region->challenge_end->format('d.m.Y, H:i') ?> Uhr</td>
            </tr>
        </table>
        <?php
        // Graphs and table for goal
        $seconds_total = $region->challenge_end->getTimestamp() - $region->challenge_start->getTimestamp();
        $seconds_remaining = $region->challenge_end_utc->getTimestamp() - time();

        if ($seconds_remaining <= 0) $time_remaining = "Zeit abgelaufen.";
        else {
            $timespan = get_beautiful_timespan($seconds_remaining);
            $time_remaining = $timespan["string"] . " verbleibend";
        }
        ?>
        <svg width="100%" height="25px">
            <g class="bar">
                <rect style="fill: hsl(120, 50%, 40%)" width="100%" height="25"></rect>
                <rect style="fill: hsl(0, 0%, 80%)" width="<?php echo (100 - $seconds_remaining / $seconds_total * 100); ?>%" height="25" x="0"></rect>
            </g>
        </svg>
        <h6 class="small"><?php echo $time_remaining; ?></h6>

        <?php
        if ($region->challenge_goal > 0) $region->challenge_goal_percent = $region->challenge_score / $region->challenge_goal * 100;
        else $region->challenge_goal_percent = 100;

        ?>
        <svg width="100%" height="25px">
            <g class="bar">
                <rect style="fill: hsl(0, 0%, 80%)" width="100%" height="25"></rect>
                <rect style="fill: hsl(120, 50%, 40%)" width="<?php echo $region->challenge_goal_percent; ?>%" height="25" x="0"></rect>
            </g>
        </svg>
        <h6 class="small"><?php echo $region->name . ": " . $region->challenge_score . ' / ' . $region->challenge_goal; ?></h6>

        <?php
        // Daily goal
        if ($seconds_remaining > 0 && $region->challenge_score < $region->challenge_goal) {
            $region->challenge_goal_today = round(($region->challenge_goal - $region->challenge_score + $region->challenge_score_today) / ceil($seconds_remaining / 86400));
            if ($region->challenge_goal_today != 0) $region->challenge_percent_today = $region->challenge_score_today / $region->challenge_goal_today * 100;
            else $region->challenge_percent_today = 100;
        ?>
            <svg width="100%" height="25px">
                <g class="bar">
                    <rect style="fill: hsl(0, 0%, 80%)" width="100%" height="25"></rect>
                    <rect style="fill: hsl(120, 50%, 40%)" width="<?php echo $region->challenge_percent_today; ?>%" height="25" x="0"></rect>
                </g>
            </svg>
            <h6 class="small">Tagesziel <?php echo $region->name . ": " . $region->challenge_score_today . ' / ' . $region->challenge_goal_today; ?></h6>
        <?php
        }
        ?>
        <br>
        <?php
        nav_exercises($mysqli);
        if ($_SESSION["exercise_id"] == 0) $title = get_region_by_id($mysqli, $_SESSION["user_region"]) . ", Gesamtpunktzahl vom " . $region->challenge_start->format('d.m.Y, H:i') . " Uhr bis " . $region->challenge_end->format('d.m.Y, H:i') . " Uhr";
        else $title = get_region_by_id($mysqli, $_SESSION["user_region"]) . ", " . get_exercise_by_id($mysqli, $_SESSION["exercise_id"])->name . " vom " . $region->challenge_start->format('d.m.Y, H:i') . " Uhr bis " . $region->challenge_end->format('d.m.Y, H:i') . " Uhr";

        display_score_table($score_array, $title, $current_date);
        ?>
        <div class="form-group">
            <a href="scoreboard_challenge.php" class="btn btn-primary btn-block">Aktualisieren</a>
        </div>
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>