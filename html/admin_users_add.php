<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if ($_SESSION["user_admin_level"] < 1) {
    header("Location: access_denied.php");
    exit;
};

if (!empty($_POST['user_name'])) {
    $user = new stdClass();
    $user->name = $_POST["user_name"];
    $user->admin_level = $_POST["user_admin_level"];
    $user->region = $_POST["user_region"];
    $user->pin = $_POST["user_pin"];
    $user = add_user($mysqli, $user);

    if ($user != false) {
        $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Nutzer "' . $_POST["user_name"] . '" erfolgreich hinzugefügt.');
        header("Location: admin_users_list.php");
        exit;
    }
}

top("Nutzer hinzufügen");
nav(build_nav($mysqli), "Nutzer verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Nutzer hinzufügen</h3>
        <br>
        <form method="post">
            <div class="form-group">
                <label for="user_name">Name</label>
                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" <?php if (isset($_POST["user_name"])) echo 'value="' . $_POST["user_name"] . '"'; ?> required autofocus>
            </div>
            <div class="form-group">
                <label for="user_pin">Pin (max. 10 Ziffern)</label>
                <input type="number" class="form-control" id="user_pin" name="user_pin" placeholder="Pin" value="<?php if (isset($_POST["user_pin"])) echo $_POST["user_pin"];
                                                                                                                    else echo "1234"; ?>" min="999" max="9999999999" required>
            </div>
            <?php
            if ($_SESSION['user_admin_level'] == 1) {
            ?>
                <div class="form-group">
                    <label for="region_select">Region wählen:</label>
                    <select class="form-control" id="region_select" name="user_region">
                        <?php echo display_region_select($mysqli, $_SESSION["user_region"]); ?>
                    </select>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="user_admin_level" id="user_admin_level2" value="1">
                    <label class="form-check-label" for="user_admin_level2">Administrator</label>
                </div>
            <?php
            } else echo '<input type="hidden" name="user_region" value="' . $_SESSION["user_region"] . '">';
            ?>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="user_admin_level" id="user_admin_level3" value="2">
                <label class="form-check-label" for="user_admin_level3">Administrator für seine Region</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="user_admin_level" id="user_admin_level1" value="0" checked>
                <label class="form-check-label" for="user_admin_level1">Nutzer</label>
            </div>
            <br>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Hinzufügen</button>
            </div>
        </form>
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>