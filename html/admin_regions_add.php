<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if ($_SESSION["user_admin_level"] != 1) {
    header("Location: access_denied.php");
    exit;
};

if (!empty($_POST['region_name'])) {
    $_SESSION["alert_array"][] = validate_region_name($mysqli, $_POST["region_name"], '0');
    $is_valid = true;
    foreach ($_SESSION["alert_array"] as $alert) if ($alert["type"] != "success") $is_valid = false;
    if ($is_valid) {
        $region = add_region($mysqli, $_POST["region_name"]);

        // Success
        array_pop($_SESSION["sites_visited"]);
        $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Region "' . $_POST["region_name"] . '" hinzugefügt.');
        header("Location: admin_regions_edit.php?region_id=" . $region->id);
        exit;
    }
}

top("Region hinzufügen");
nav(build_nav($mysqli), "Regionen verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Region hinzufügen</h3>
        <br>
        <form method="post">
            <div class="form-group">
                <label for="region_name">Name der Region</label>
                <input type="text" class="form-control" id="region_name" name="region_name" placeholder="Name der Region" <?php if (isset($_POST["region_name"])) echo 'value="' . $_POST["region_name"] . '"'; ?> autofocus>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Hinzufügen</button>
            </div>
        </form>
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>