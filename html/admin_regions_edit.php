<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if ($_SESSION["user_admin_level"] < 1) {
    header("Location: access_denied.php");
    exit;
};

if (!isset($_GET["region_id"]) || !is_numeric($_GET["region_id"]) || $_GET["region_id"] < 1) {
    $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Keine gültige Region ausgewählt.");
    header("Location: admin_regions_list.php");
    exit;
}

// For region-admins
if ($_SESSION["user_admin_level"] == 2) $_GET["region_id"] = $_SESSION["user_region"];

// Check if a valid region was selected
$result = $mysqli->query("SELECT id, name, challenge_goal, CONVERT_TZ(challenge_start, 'UTC', 'Europe/Berlin') as challenge_start, CONVERT_TZ(challenge_end, 'UTC', 'Europe/Berlin') as challenge_end FROM regions WHERE id = $_GET[region_id]");
if ($result->num_rows < 1) {
    $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Keine gültige Region ausgewählt.");
    header("Location: admin_regions_list.php");
    exit;
} else {
    $region = $result->fetch_object();
    $region->challenge_start = new DateTime($region->challenge_start);
    $region->challenge_end = new DateTime($region->challenge_end);
}

if (!empty($_POST['action'])) {
    switch ($_POST['action']) {
        case "delete_region":
            if ($region->id != "1") {
                $query_user = $mysqli->query("SELECT * FROM users WHERE region = $region->id") or die($mysqli->error);
                while ($user = $query_user->fetch_object()) {
                    delete_user($mysqli, $user->id);
                }
                $mysqli->query("DELETE FROM users WHERE region = $region->id") or die($mysqli->error);
                $mysqli->query("DELETE FROM stats WHERE region_id = $region->id") or die($mysqli->error);
                $mysqli->query("DELETE FROM exercises WHERE region_id = $region->id") or die($mysqli->error);
                $mysqli->query("DELETE FROM regions WHERE id = $region->id") or die($mysqli->error);
                if ($_SESSION["user_region"] == $region->id) $_SESSION["user_region"] = 1;

                $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Region "' . $region->name . '" gelöscht.');
                header("Location: admin_regions_list.php");
                exit;
            }
            break;

        case "edit_region":
            // Update name
            if ($_POST["region_name"] != "" && $_POST["region_name"] != $region->name) {
                $result = $mysqli->query("SELECT * FROM regions WHERE name LIKE '$_POST[region_name]' AND id != $region->id");
                if ($result->num_rows != 0) $_SESSION["alert_array"][] = array("message" => "Eine Region mit dem Namen $_POST[region_name] existiert bereits.", "type" => "danger");
                else {
                    $mysqli->query("UPDATE regions SET name = '$_POST[region_name]' WHERE id = $region->id");
                    if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                    else $_SESSION["alert_array"][] = array("type" => "success", "message" => "Die Region wurde umbenannt.");
                }
            }

            // Update dates
            if (!empty($_POST["challenge_start"]) && !empty($_POST["challenge_end"]) && ($_POST["challenge_start"] != $region->challenge_start->format("Y-m-d H:i:s") || $_POST["challenge_end"] != $region->challenge_end->format("Y-m-d H:i:s"))) {
                if (strtotime($_POST["challenge_start"]) === false) $_SESSION["alert_array"][] = array("type" => "danger", "message" => "Das Startdatum ist ungültig. Beachte bitte das passende Datumsformat:<br><br>Jahr-Monat-Tag Stunden:Minuten:Sekunden");
                elseif (strtotime($_POST["challenge_end"]) === false) $_SESSION["alert_array"][] = array("type" => "danger", "message" => "Das Enddatum ist ungültig. Beachte bitte das passende Datumsformat:<br><br>Jahr-Monat-Tag Stunden:Minuten:Sekunden");
                elseif (strtotime($_POST["challenge_end"]) - strtotime($_POST["challenge_start"]) < 600) $_SESSION["alert_array"][] = array("type" => "danger", "message" => "Das Startdatum muss mindestens 10 Minuten vor dem Enddatum liegen.");
                else {
                    $mysqli->query("UPDATE regions SET challenge_start = CONVERT_TZ('$_POST[challenge_start]', '$timezone_user', 'UTC'), challenge_end = CONVERT_TZ('$_POST[challenge_end]', '$timezone_user', 'UTC') WHERE id = $region->id");
                    if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                    else $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Zeitraum der Herausforderung aktualisiert.');
                }
            }

            // Update goals
            if ($_POST["challenge_goal"] != "" && $_POST["challenge_goal"] != $region->challenge_goal) {
                if (!is_numeric($_POST["challenge_goal"])) $_SESSION["alert_array"][] = array("type" => "danger", "message" => "Das Ziel ist keine gültige Zahl.");
                elseif ($_POST["challenge_goal"] < 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => "Das Ziel darf nicht negativ sein.");
                else {
                    $mysqli->query("UPDATE regions SET challenge_goal = $_POST[challenge_goal] WHERE id = '$region->id';");
                    if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
                    else $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Ziel aktualisiert.');
                }
            }

            header("Location: admin_regions_edit.php?region_id=" . $region->id);
            exit;
            break;
    }
}

// Select all users with that region
$result = $mysqli->query("SELECT * from users WHERE region = $region->id ORDER BY name");
while ($user = $result->fetch_object()) {
    $users_array[] = $user;
}

top("Regionen verwalten");
nav(build_nav($mysqli), "Regionen verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Region "<?php echo $region->name; ?>" verwalten</h3>
        <br>
        <?php
        if ($region->id != "1" || $region->id == $_SESSION["user_region"]) {
        ?>
            <form method="post">
                <?php
                if ($region->id != "1") {
                ?>
                    <div class="form-group">
                        <label for="region_name">Name der Region</label>
                        <input type="text" class="form-control" id="region_name" name="region_name" placeholder="<?php echo $region->name; ?>">
                    </div>
                    <br>
                <?php
                }
                if ($region->id == $_SESSION["user_region"]) {
                ?>
                    <h4>Herausforderung</h4>
                    <div class="form-group">
                        <label for="challenge_goal">Ziel</label>
                        <input type="number" class="form-control" id="challenge_goal" name="challenge_goal" value="<?php echo $region->challenge_goal; ?>">
                    </div>
                    <div class="form-group">
                        <label for="challenge_start">Startzeitpunkt</label>
                        <input type="datetime-local" class="form-control" id="challenge_start" name="challenge_start" value="<?php echo $region->challenge_start->format("Y-m-d H:i:s"); ?>">
                    </div>
                    <div class="form-group">
                        <label for="challenge_end">Endzeitpunkt</label>
                        <input type="datetime-local" class="form-control" id="challenge_end" name="challenge_end" value="<?php echo $region->challenge_end->format("Y-m-d H:i:s"); ?>">
                    </div>
                <?php
                }
                ?>
                <input type="hidden" name="action" value="edit_region">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Änderungen übernehmen</button>
                </div>
            </form>
            <div class="form-group">
                <a href="admin_exercises_list.php" class="btn btn-outline-secondary btn-block">Übungen verwalten</a>
            </div>
        <?php
        }
        if ($region->id != "1" && $_SESSION["user_admin_level"] == 1) {
        ?>
            <form method="post">
                <input type="hidden" name="action" value="delete_region">
                <div class="form-group">
                    <button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Willst du die Region wirklich löschen? Alle Nutzer der Region werden ebenfalls gelöscht, sowie alle Übungen und Einträge der Region. Dies kann nicht rückgängig gemacht werden!')">Region löschen</button>
                </div>
            </form>
        <?php
        }
        if (isset($users_array)) {
        ?>
            <br>
            <table class="table">
                <caption>Nutzer der Region <?php echo $region->name; ?></cation>
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nutzer</th>
                        <th scope="col">Berechtigung</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($users_array as $user) {
                        echo '<tr><td scope="row">' . $i . '</td>';
                        if (may_edit_user($user)) echo '<td scope="row"><a href="admin_users_edit.php?user_id=' . $user->id . '">' . $user->name . '</a></td>';
                        else echo '<td scope="row">' . $user->name . '</td>';
                        echo '<td>' . get_admin_description($mysqli, $user) . '</td></tr>';
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        <?php
        }
        echo back_button();
        ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>