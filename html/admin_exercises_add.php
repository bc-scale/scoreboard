<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

if ($_SESSION["user_admin_level"] < 1) {
    header("Location: access_denied.php");
    exit;
};

if (!empty($_POST["exercise_name"])) {
    $check_exercise_name = $mysqli->query("SELECT * FROM exercises WHERE region_id = $_SESSION[user_region] AND name LIKE '$_POST[exercise_name]';");
    if ($check_exercise_name->num_rows > 0) $_SESSION["alert_array"][] = array("type" => "warning", "message" => "Eine Übung mit diesem Namen existiert bereits.");
    else {
        $mysqli->query("INSERT INTO exercises(name, description, region_id) VALUES ('$_POST[exercise_name]', '$_POST[exercise_description]', '$_SESSION[user_region]');");

        // Success
        if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
        else {
            $result_exercise = $mysqli->query("SELECT * FROM exercises WHERE region_id = $_SESSION[user_region] AND name LIKE '$_POST[exercise_name]';");
            $exercise = $result_exercise->fetch_object();
            $result_user = $mysqli->query("SELECT id FROM users WHERE region = $_SESSION[user_region];");
            while ($user = $result_user->fetch_object()) {
                $mysqli->query("INSERT INTO user_goals(user_id, exercise_id) VALUES ('$user->id', '$exercise->id');");
            }
            if ($mysqli->errno != 0) $_SESSION["alert_array"][] = array("type" => "danger", "message" => $mysqli->error);
            else {
                $_SESSION["alert_array"][] = array("type" => "success", "message" => 'Übung "' . $_POST["exercise_name"] . '" hinzugefügt.');
                array_pop($_SESSION["sites_visited"]);
                header("Location: admin_exercises_list.php");
                exit;
            }
        }
    }
}

top("Übung hinzufügen");
nav(build_nav($mysqli), "Übungen verwalten");
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Übung hinzufügen</h3>
        <br>
        <form method="post">
            <div class="form-group">
                <label for="exercise_name">Name der Übung</label>
                <input type="text" class="form-control" id="exercise_name" name="exercise_name" placeholder="Name der Übung" <?php if (isset($_POST["exercise_name"])) echo 'value="' . $_POST["exercise_name"] . '"'; ?> autofocus>
            </div>
            <div class="form-group">
                <label for="exercise_description">Beschreibung</label>
                <textarea class="form-control" id="exercise_description" name="exercise_description" rows="3" maxlength="1000"><?php if (isset($_POST["exercise_description"])) echo '' . $_POST["exercise_description"]; ?></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Hinzufügen</button>
            </div>
        </form>
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
bot();
?>