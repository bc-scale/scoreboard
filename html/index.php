<?php
session_start();
include('inc/config.php');
include('inc/functions.php');
$version = update_db($mysqli);

// Logout
if (isset($_GET["action"]) && $_GET["action"] == "logout") {
    foreach ($_SESSION as $key => $value) if ($key != "alert_array") unset($_SESSION[$key]);
    $_SESSION["alert_array"][] = array("message" => "Du wurdest erfolgreich abgemeldet.", "type" => "success");
}

// Forward if user is already logged in
elseif (isset($_SESSION["user_id"])) {
    header("Location: add_entry.php");
    exit;
}

// Login
elseif (!empty($_POST["name"]) && !empty($_POST["pin"] && is_numeric($_POST["pin"]))) {
    $_POST["name"] = trim($_POST["name"]);
    $result = $mysqli->query("SELECT * FROM users WHERE name LIKE '$_POST[name]'");
    if ($result->num_rows > 0) {
        $user = $result->fetch_object();
        if (password_verify($_POST["pin"], $user->pin)) {
            $_SESSION["user_id"] = $user->id;
            $_SESSION["user_name"] = $user->name;
            $_SESSION["user_region"] = $user->region;
            $_SESSION["user_admin_level"] = $user->admin_level;
            $_SESSION["sites_visited"] = array("/index.php?action=logout");
            $_SESSION["exercise_id"] = 0;
            $_SESSION["version"] = $version;

            if ($_POST["pin"] == 1234) {
                header("Location: user_settings.php");
                $_SESSION["alert_array"][] = array("message" => "Bitte ändere deinen PIN.", "type" => "warning");
            } else header("Location: add_entry.php");
            exit;
        } else $_SESSION["alert_array"][] = array("message" => "Das hat nicht geklappt. Falscher Pin.", "type" => "danger");
    } else $_SESSION["alert_array"][] = array("message" => "Das hat nicht geklappt. Dieser Nutzer existiert nicht." . $_POST["name"], "type" => "danger");
}

include('inc/frame_functions.php');
include('inc/nav_functions.php');

top("Login");
nav();
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <h3>Anmeldung</h3>
        <form action="index.php" method="post">
            <div class="form-group">
                <label for="InputName">Nutzername</label>
                <input type="text" class="form-control" id="InputName" name="name" placeholder="Nutzername" <?php if (isset($_POST["name"])) echo 'value="' . $_POST["name"] . '"'; ?> autofocus>
            </div>
            <div class="form-group">
                <label for="InputPin">Pin</label>
                <input type="number" class="form-control" id="InputPin" name="pin" placeholder="Pin">
            </div>
            <button type="submit" class="btn btn-primary btn-block">Anmelden</button>
        </form>
        <br>
    </div>
    <div class="col-md-3"></div>
</div>

<?php
// Tipps & Tricks
$tipps = array(
    'Dein persönliches Tagesziel kannst du in den Accounteinstellungen anpassen.'
);
echo '<div class="row"><div class="col-md-6 offset-md-3"><div class="alert alert-info" role="alert">Tipp:<br>' . $tipps[rand(0, count($tipps) - 1)] . '</div></div></div>';
bot();
?>