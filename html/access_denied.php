<?php
include('inc/functions.php');
include('inc/config.php');
include('inc/frame_functions.php');
include('inc/nav_functions.php');

start($mysqli);

$_SESSION["alert_array"][] = array("type" => "danger", "message" => "Zugriff verweigert. Du verfügst nicht über ausreichende Berechtigungen um diese Seite aufzurufen.");

top("Zugang verweigert");
nav(build_nav($mysqli));
start_main();
?>
<div class="row">
    <div class="col offset-md-3">
        <?php echo back_button(); ?>
    </div>
    <div class="col-md-3"></div>
</div>
<?php
bot();
array_pop($_SESSION["sites_visited"]);
